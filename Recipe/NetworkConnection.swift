//
//  NetworkConnection.swift
//  Recipe
//
//  Created by Amit on 16/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


enum NetworkRequestType {
    case  GetRecipeList
    case  GetRecipeDetail
}

protocol NetworkConnection_Delegate {
    
    func networkConnectionDidSuccessWithResponse(response:AnyObject,requestType:NetworkRequestType)
    func networkConnectionDidFailedWithErrorResponse(response:AnyObject,requestType:NetworkRequestType)
}

class NetworkConnection: NSObject {
    
    var delegate:NetworkConnection_Delegate?
    
    init(delegate_:NetworkConnection_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    
    func networkRequestToGetRecipeList(postParameters:NSDictionary) {
        
        let url: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.Search
        
        Alamofire.request(.POST, url, parameters:postParameters as? [String : AnyObject] , encoding:.URL).responseJSON
            { response in switch response.result {
                
            case .Success(let JSON):
                
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    lDelegate.networkConnectionDidSuccessWithResponse(JSON, requestType: .GetRecipeList)
                }
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    lDelegate.networkConnectionDidFailedWithErrorResponse(error, requestType: .GetRecipeList)
                }
                }
        }
    }
    
    
    func networkRequestToGetRecipeDetail(postParameters:NSDictionary) {
        
        let url: String = Constants.NetworkConnection.BaseUrl + Constants.NetworkConnection.Get
        
        Alamofire.request(.POST, url, parameters:postParameters as? [String : AnyObject] , encoding:.URL).responseJSON
            { response in switch response.result {
                
            case .Success(let JSON):
                
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    
                    lDelegate.networkConnectionDidSuccessWithResponse(JSON, requestType: .GetRecipeDetail)
                }
            case .Failure(let error):
                if let lDelegate:NetworkConnection_Delegate = self.delegate {
                    lDelegate.networkConnectionDidFailedWithErrorResponse(error, requestType: .GetRecipeDetail)
                }
                }
        }
    }
    
    
    
}
 
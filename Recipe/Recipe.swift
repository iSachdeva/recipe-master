//
//  Recipe.swift
//  Recipe
//
//  Created by Amit on 17/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation

class Recipe {
    
    var recipeId:String?
    var title:String?
    var publisher:String?
    var imageUrl:String?
    
    
  init(recipeId:String, title:String, publisher:String, imageUrl:String) {
    
    self.recipeId = recipeId
    self.title = title
    self.publisher = publisher
    self.imageUrl = imageUrl
   
    }
}


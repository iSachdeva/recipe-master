//
//  RecipeDetail.swift
//  Recipe
//
//  Created by Amit on 17/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation

class RecipeDetail{
    
    var recipeId:String?
    var title:String?
    var publisher:String?
    var ingredients:String?

    var imageUrl:String?
    var publisherUrl:String?
    var sourceUrl:String?
    var f2fUrl:String?
  
    var socialRank:NSNumber?
    
    
    init (recipeId:String, title:String, publisher:String, ingredients:String, imageUrl:String, publisherUrl:String, sourceUrl:String, f2fUrl:String, socialRank:NSNumber) {
        
        self.recipeId = recipeId
        self.title = title
        self.publisher = publisher
        self.ingredients = ingredients
        
        self.imageUrl = imageUrl
        self.publisherUrl = publisherUrl
        self.sourceUrl = sourceUrl
        self.f2fUrl = f2fUrl
        self.socialRank = socialRank
        
    }
}
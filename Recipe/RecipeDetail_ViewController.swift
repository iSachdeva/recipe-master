
//
//  RecipeDetail_ViewController.swift
//  Recipe
//
//  Created by Amit on 17/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class RecipeDetail_ViewController: UIViewController,RecipeEngine_Delegate,UIAlertViewDelegate {
    
    //MARK:- View Propties
    @IBOutlet var baseScrollView:UIScrollView!
    
    @IBOutlet var recipeImageView:UIImageView!
    @IBOutlet var rankLabel:UILabel!
    @IBOutlet var rankImageView:UIImageView!
    
    @IBOutlet var titleLabel:UILabel!
    @IBOutlet var publisherLabel:UILabel!
    @IBOutlet var firstSepratorView:UIView!
    @IBOutlet var secondSepratorView:UIView!
    @IBOutlet var ingredientLabel:UILabel!
    @IBOutlet var ingredientListLabel:UILabel!
    
    @IBOutlet var urlButton:UIButton!
    
    //MARK:- Data Propties
    var recipeId:String?
    var recipeEngine:RecipeEngine!
    var recipeDetail:RecipeDetail?
    
    //MARK:- View Lifecycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipeEngine  = RecipeEngine(delegate_: self)
        if let recipeIdTemp = recipeId {
            recipeEngine.getRecipeDetail(recipeIdTemp)
            KVNProgress.show()
        }
        
        baseScrollView.hidden = true
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- UI Helper Method
    func populateRecipeDetail()  {
        
        baseScrollView.hidden = false
        
        self.loadDetailImage()
        
        let rank = recipeDetail?.socialRank
        if let rankValue = rank {
            rankLabel.text = String(rankValue.integerValue)
        } else {
            rankLabel.text = ""
            rankImageView.hidden = true
        }
        
        titleLabel.text = recipeDetail?.title
        titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, recipeImageView.frame.maxY + 10, titleLabel.frame.size.width, titleLabel.frame.size.height)
        titleLabel.sizeToFit()
        
        publisherLabel.text = recipeDetail?.publisher
        publisherLabel.frame = CGRectMake(publisherLabel.frame.origin.x, titleLabel.frame.maxY + 7, publisherLabel.frame.size.width, publisherLabel.frame.size.height)
        publisherLabel.sizeToFit()
        
        firstSepratorView.frame = CGRectMake(firstSepratorView.frame.origin.x, publisherLabel.frame.maxY + 10, firstSepratorView.frame.size.width, firstSepratorView.frame.size.height)
        
        ingredientLabel.frame = CGRectMake(ingredientLabel.frame.origin.x, firstSepratorView.frame.maxY + 10, ingredientLabel.frame.size.width, ingredientLabel.frame.size.height)
        
        ingredientListLabel.text = recipeDetail?.ingredients
        
        ingredientListLabel.frame = CGRectMake(ingredientListLabel.frame.origin.x, ingredientLabel.frame.maxY + 10, ingredientListLabel.frame.size.width, ingredientListLabel.frame.size.height)
        ingredientListLabel.sizeToFit()
        
        secondSepratorView.frame = CGRectMake(secondSepratorView.frame.origin.x, ingredientListLabel.frame.maxY + 10, secondSepratorView.frame.size.width, secondSepratorView.frame.size.height)
        
        
        let isValidUrl =  recipeEngine.isaValidSourceUrlAvailable(recipeDetail?.sourceUrl)
        if isValidUrl {
            urlButton.frame = CGRectMake(urlButton.frame.origin.x, secondSepratorView.frame.maxY + 10, urlButton.frame.size.width, urlButton.frame.size.height)
            
            baseScrollView.contentSize = CGSizeMake(baseScrollView.frame.size.width, urlButton.frame.maxY + 10)
            
        } else {
            urlButton.hidden = true
            
            baseScrollView.contentSize = CGSizeMake(baseScrollView.frame.size.width, secondSepratorView.frame.maxY + 10)
        }
    }
    
    func loadDetailImage() {
        
        if let imageUrlStr = recipeDetail?.imageUrl {
            let imageUrl = NSURL(string: imageUrlStr)
            
            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
            }
            
            let placeHolderImage = UIImage(named: "listPlaceholder")
            self.recipeImageView!.sd_setImageWithURL(imageUrl, placeholderImage: placeHolderImage, completed: block)
        }
    }
    
    //MARK:- IBAction Methods
    @IBAction func urlButtonClicked(sender:UIButton) {
        
        let alertView = UIAlertView(title: "Do you allow app to open Safari?", message: "", delegate: self, cancelButtonTitle: "NO", otherButtonTitles: "Open")
        alertView.tag = 1
        alertView.show()
    }
    
    //MARK:- UIAlertView Delegate Method
    func alertView(alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if (alertView.tag == 1) && (buttonIndex == 1) {
            let sourceUrl = recipeDetail?.sourceUrl
            let url = NSURL(string: sourceUrl!)
            if let urlValue = url {
                UIApplication.sharedApplication().openURL(urlValue)
            } else {
                KVNProgress.showErrorWithStatus("Sorry!! No More info available at this moment")
            }
        }
    }
    
    
    //MARK:- Recipe Engine Delegate method
    func didRequestFinishedWithSuccessResponse(response: AnyObject, requestType: RecipeEngineRequestType) {
        
        switch requestType {
            
        case .GetRecipeDetail:
            
            recipeDetail = response as? RecipeDetail
            
            self.populateRecipeDetail()
            KVNProgress.dismiss()
            
            break;
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse: AnyObject, requestType: RecipeEngineRequestType) {
        
        switch requestType {
            
        case .GetRecipeDetail:
            
            KVNProgress.dismiss()
            let alert:UIAlertView = UIAlertView(title: "Error", message: "Request has been failed. Please try again.", delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            
            break;
            
        default:
            break
        }
    }
    
}

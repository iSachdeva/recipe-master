//
//  RecipeEngine.swift
//  Recipe
//
//  Created by Amit on 16/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import Foundation
import UIKit


enum RecipeEngineRequestType {
    
    case  GetRecipeList
    case  GetRecipeDetail
}

protocol RecipeEngine_Delegate {
    
    func didRequestFinishedWithSuccessResponse(response:AnyObject, requestType:RecipeEngineRequestType)
    func didRequestFailedWithErrorResponse(errorResponse:AnyObject, requestType:RecipeEngineRequestType)
}

class RecipeEngine: NSObject,NetworkConnection_Delegate {
    
    var delegate:RecipeEngine_Delegate?
    //  var networkConnection:NetworkConnection!
    
    //MARK:- init()
    init(delegate_:RecipeEngine_Delegate) {
        super.init()
        self.delegate = delegate_
    }
    
    //MARK:- API's
    func getRecipeList(pageNumber:NSInteger) {
        
        let parmeters = ["key":Constants.Key.ApiKey,"page":pageNumber]
        let networkConnection = NetworkConnection(delegate_: self)
        networkConnection.networkRequestToGetRecipeList(parmeters)
    }
    
    func getRecipeDetail(recipeId:String) {
        
        let parmeters = ["key":Constants.Key.ApiKey,"rId":recipeId]
        let networkConnection = NetworkConnection(delegate_: self)
        networkConnection.networkRequestToGetRecipeDetail(parmeters)
    }

    
    //MARK:- NetworkConnection Delegate
    func networkConnectionDidSuccessWithResponse(response: AnyObject, requestType: NetworkRequestType) {
        
        switch requestType
        {
            case .GetRecipeList:
                
                let recipesResponse = response.objectForKey("recipes") as! NSArray
                var recipes = [Recipe]()
                if recipesResponse.count > 0  {
                   recipes.appendContentsOf(self.parseRecipeListResponse(recipesResponse) as [Recipe])
                }
                
                self.delegate?.didRequestFinishedWithSuccessResponse(recipes, requestType: .GetRecipeList)
            break
            
            case .GetRecipeDetail:
                
                let recipe = response.objectForKey("recipe") as! NSDictionary
                let recipeDetails =   self.parseRecipeDetailResponse(recipe)
                
                self.delegate?.didRequestFinishedWithSuccessResponse(recipeDetails, requestType: .GetRecipeDetail)
            break
            
        }
    }
    
    
    func networkConnectionDidFailedWithErrorResponse(response: AnyObject, requestType: NetworkRequestType) {
        switch requestType
        {
        case .GetRecipeList:
            
            self.delegate?.didRequestFailedWithErrorResponse(response, requestType: .GetRecipeList)
            break;
            
        case .GetRecipeDetail:
            self.delegate?.didRequestFailedWithErrorResponse(response, requestType: .GetRecipeDetail)

            break;
            
        }
    }
    
    //MARK:- Helper Methods
    func parseRecipeListResponse(recipes:NSArray) -> [Recipe] {
        
        var recipesList = [Recipe]()
        
        for i in 0...recipes.count-1 {
            
            let aRecipe = recipes.objectAtIndex(i) as! NSDictionary
            let recipeId = aRecipe.objectForKey("recipe_id") as! String
            let recipeTitle = aRecipe.objectForKey("title") as! String
            let recipePublisher = aRecipe.objectForKey("publisher") as! String
            let recipeImageUrl = aRecipe.objectForKey("image_url") as! String

            let aRecipeModal = Recipe(recipeId: recipeId, title: recipeTitle, publisher: recipePublisher, imageUrl: recipeImageUrl)
            recipesList.append(aRecipeModal)
        }
        
        return recipesList
    }
    
    
    func parseRecipeDetailResponse(aRecipeDetail:NSDictionary) -> RecipeDetail {

        let recipeId = aRecipeDetail.objectForKey("recipe_id") as! String

        let title = aRecipeDetail.objectForKey("title") as! String
        let publisher = aRecipeDetail.objectForKey("publisher") as! String
        let publisherUrl = aRecipeDetail.objectForKey("publisher_url") as! String
        let sourceUrl = aRecipeDetail.objectForKey("source_url") as! String
        let f2fUrl = aRecipeDetail.objectForKey("f2f_url") as! String
        let imageUrl = aRecipeDetail.objectForKey("image_url") as! String
        let socialRank = aRecipeDetail.objectForKey("social_rank") as! NSNumber
        
        let ingredients = aRecipeDetail.objectForKey("ingredients") as! NSArray
        
        var ingredientStr = ""
        
        for i in 0...ingredients.count-1 {
            let aIngredient = ingredients.objectAtIndex(i) as! String
            if i == ingredients.count-1 {
                ingredientStr = ingredientStr + aIngredient
            } else if (i == 0){
                ingredientStr = "•" + " " + aIngredient
            } else {
                ingredientStr = ingredientStr + aIngredient + "\n\n• "

            }
        }
        
        let recipeDetail = RecipeDetail(recipeId: recipeId, title: title, publisher: publisher, ingredients: ingredientStr, imageUrl: imageUrl, publisherUrl: publisherUrl, sourceUrl: sourceUrl, f2fUrl: f2fUrl, socialRank: socialRank)
        
        return recipeDetail
    }
    
    
    func isaValidSourceUrlAvailable(urlStr:String?) -> Bool {
        
        var isValid:Bool = false
        
        if let sourceUrlValue = urlStr {
            let url = NSURL(string: sourceUrlValue)
            if let urlValue = url {
                print(urlValue)
                isValid = true
            } else {
                isValid = false
            }
        } else {
            isValid = false
        }
        
        return isValid
    }
}
    
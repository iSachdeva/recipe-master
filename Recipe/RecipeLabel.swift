//
//  RecipeLabel.swift
//  Recipe
//
//  Created by Amit on 17/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class RecipeLabel: UILabel {

    override func drawTextInRect(rect: CGRect) {
        let insets: UIEdgeInsets = UIEdgeInsets(top: 0.0, left: 5.0, bottom: 0.0, right: 3.0)
        super.drawTextInRect(UIEdgeInsetsInsetRect(rect, insets))
        
    }
    
}

//
//  RecipeList_TableViewController.swift
//  Recipe
//
//  Created by Amit on 16/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit

class RecipeList_TableViewController: UITableViewController,RecipeEngine_Delegate {
    
    var recipeEngine:RecipeEngine!
    var recipeList = [Recipe]()
    var currentLoadedPage:NSInteger!
    
    //MARK:- View LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        recipeEngine  = RecipeEngine(delegate_: self)
        currentLoadedPage = 1
        self.loadRecipeList(currentLoadedPage)
        
        self.refreshControl?.addTarget(self, action: #selector(RecipeList_TableViewController.handleRefresh(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK- Load New/More Request
    func loadRecipeList(page:NSInteger) {
        
        KVNProgress.show()
        recipeEngine.getRecipeList(page)
    }
    
    //MARK:- UIRefreshControl Selector
    func handleRefresh(refreshControl: UIRefreshControl) {
        currentLoadedPage = 1
        recipeList.removeAll()
        refreshControl.endRefreshing()
        self.tableView.reloadData()
        
        self.loadRecipeList(currentLoadedPage)
    }
    
    //MARK:- UITableView Delegate Methods
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeList.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let recipeCell:Recipe_TableViewCell = tableView.dequeueReusableCellWithIdentifier("recipeCellIdentifier", forIndexPath: indexPath) as! Recipe_TableViewCell
     
        let aRecipe = recipeList[indexPath.row]
        recipeCell.populateRecipeCell(aRecipe)
        
        return recipeCell
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.row == recipeList.count - 1 {
            self.loadRecipeList(currentLoadedPage + 1)
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let aRecipe = recipeList[indexPath.row]
        self.performSegueWithIdentifier("RecipeListToDetailScreen_Id", sender: aRecipe.recipeId)
    }
    
    
    //MARK:- Recipe Engine Delegate method
    
    func didRequestFinishedWithSuccessResponse(response: AnyObject, requestType: RecipeEngineRequestType) {
        
        switch requestType {
        
            case .GetRecipeList:
            
                recipeList.appendContentsOf(response as![Recipe])
                currentLoadedPage = currentLoadedPage + 1
          
                KVNProgress.dismiss()
                self.tableView.reloadData()
            
                break;
            
        default:
            break
        }
    }
    
    func didRequestFailedWithErrorResponse(errorResponse: AnyObject, requestType: RecipeEngineRequestType) {
     
        switch requestType {
      
            case .GetRecipeList:
            
                KVNProgress.dismiss()
                let alert:UIAlertView = UIAlertView(title: "Error", message: "Request has been failed. Please try again.", delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            
                break;
        default:
            break
        }
    }
    
    
    //MARK:- Navigation Method
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "RecipeListToDetailScreen_Id" {
            let recipeDetailViewController = segue.destinationViewController as! RecipeDetail_ViewController
            recipeDetailViewController.recipeId = sender as? String
        }
        
    }
    
}

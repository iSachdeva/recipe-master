//
//  Recipe_TableViewCell.swift
//  Recipe
//
//  Created by Amit on 16/04/2016.
//  Copyright © 2016 Global Line Network. All rights reserved.
//

import UIKit
import Alamofire


class Recipe_TableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel:RecipeLabel?
    @IBOutlet weak var publisherLabel:RecipeLabel?
    @IBOutlet weak var recipeImageView:UIImageView?

    
    func populateRecipeCell(aRecipe:Recipe) {
        
        if let recipeTitle = aRecipe.title {
            self.titleLabel?.text = recipeTitle
        } else {
            self.titleLabel?.text = ""
        }
        
        if let recipePublisher = aRecipe.publisher {
            self.publisherLabel?.text = recipePublisher
        } else {
            self.publisherLabel?.text = ""
        }
        
        if let imageUrlStr = aRecipe.imageUrl {
            let imageUrl = NSURL(string: imageUrlStr)
            
            let block: SDWebImageCompletionBlock! = {(image: UIImage!, error: NSError!, cacheType: SDImageCacheType!, imageURL: NSURL!) -> Void in
            }
            
            let placeHolderImage = UIImage(named: "listPlaceholder")
            self.recipeImageView!.sd_setImageWithURL(imageUrl, placeholderImage: placeHolderImage, completed: block)
        }
        
    }
}

